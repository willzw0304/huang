from django.contrib import admin
from django.urls import path
from . import views
from finance.views import *

app_name='finance'
urlpatterns = [
	path('',views.index,name="finance_index"),
	path('addorder/',views.AddOrder,name="addorder"),
    path('searchorder/',views.SearchOrderView.as_view(),name="searchorder"),
    path('change_order/',views.change_order,name="change_order"),
    path('delete_order/',views.delete_order,name="delete_order"),
    path('orderdetail/',views.orderdetail,name="orderdetail"),
    path('addproduce/',views.AddProduceView,name="addproduce"),
    path('deleteproduce/',views.deleteproduce,name="deleteproduce"),
    path('changeproduce/<int:nid>/',views.changeproduce,name="changeproduce"),
    path('searchproduce/',views.SearchProduceView.as_view(),name="searchproduce"),
    path('searchoffer/',views.SearchOfferView.as_view(),name="searchoffer"),
    path('offerdetail/',views.SearchOfferDetailView.as_view(),name="searchofferdetail"),
    path('searchorderdetail/',views.SearchOrderDetailView.as_view(),name="searchorderdetail"),
    path('addoffer/',views.AddofferView.as_view(),name="addoffer"),
    path('dooffer/',views.dooffer,name="dooffer"),
    path('deloffer/<str:offerid>/',views.deloffer,name="deloffer"),
    path('delofferdetail/',views.delofferdetail,name="delofferdetail"),
    path('changeoffer/',views.changeoffer,name="changeoffer"),
    path('ordercollectionadd/<str:order_id>/',views.OrderCollectionCreate.as_view(),name="addordercollection"),
    path('ordercollectionupdate/<str:order_id>/<int:pk>/',views.OrderCollectionUpdate.as_view(),name="updateordercollection"),
    path('ordercollectiondelete/<str:order_id>/<int:pk>/',views.OrderCollectionDelete.as_view(),name="deleteordercollection"),
    path('ordercostadd/<str:order_id>/',views.OrderCostCreate.as_view(),name="addordercost"),
    path('ordercostupdate/<str:order_id>/<int:pk>/',views.OrderCostUpdate.as_view(),name="updateordercost"),
    path('ordercostdelete/<str:order_id>/<int:pk>/',views.OrderCostDelete.as_view(),name="deleteordercost"),
    path('orderdetailadd/<str:order_id>/',views.OrderDetailCreate.as_view(),name="addorderdetail"),
    path('orderdetailupdate/<str:order_id>/<int:pk>/',views.OrderDetailUpdate.as_view(),name="updateorderdetail"),
    path('orderdetaildelete/<str:order_id>/<int:pk>/',views.OrderDetailDelete.as_view(),name="deleteorderdetail"),
]

