#	-*-	coding:	UTF-8	-*-
from django.db import	models

#	Create your	models here.
class	customer(models.Model):
		customer_name	=	models.CharField(max_length=70,null=False,db_column='客户姓名',verbose_name='客户姓名')
		age	=	models.IntegerField(blank=True,default=0,verbose_name='客户年龄')
		country=models.CharField(max_length=170,blank=True,default='其他',verbose_name='国家')
		contact=models.CharField(max_length=170,null=True,default='未知',verbose_name='联系方式')
		createdatetime=models.DateTimeField(auto_now=True,verbose_name='创建时间')
		
		class	Meta:
				verbose_name='客户信息表'
				verbose_name_plural=verbose_name
		def	__str__(self):
				return self.customer_name
		

		

class	produceclass(models.Model):
		produce_class_name=models.CharField(max_length=20,default='其他',verbose_name='产品类型')
		
		class	Meta:
				verbose_name='产品类型'
				verbose_name_plural=verbose_name
		
		def	__str__(self):
				return self.produce_class_name

class	produce(models.Model):
		unit_choices = (
    		('个', '个'),
    		('台', '台'),)
		category=models.CharField(max_length=100,default='未知',verbose_name='类目')
		producename=models.CharField(max_length=100,default='未知',verbose_name='产品名称')
		describe=models.CharField(max_length=100,default='未知',verbose_name='详细描述')
		pronumber=models.CharField(max_length=100,default='其他',verbose_name='货号')
		factorynumber=models.CharField(max_length=100,default='其他',verbose_name='工厂货号')
		icon = models.ImageField(verbose_name='商品样图',upload_to='goods_icon/', null=True, blank=True)
		manufacturer=models.CharField(max_length=100,default='未知',verbose_name='厂家')
		size=models.CharField(max_length=100,default='未知',verbose_name='尺寸',help_text='涉及长宽高，不同厚度')
		weight=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='单个产品重量g')
		packdetailsnote=models.CharField(max_length=100,default='未知',verbose_name='包装详情备注')
		eachpacknum=models.DecimalField(max_digits=15,decimal_places=5,default=1,verbose_name='每件包装数量')
		plong=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='长')
		pwide=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='宽')
		phigh=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='高')
		factoryprice=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='工厂价格（出厂价）')
		unit=models.CharField(choices=unit_choices,max_length=10,default='个',verbose_name='单位')
		freightdesc=models.CharField(max_length=100,default='未知',verbose_name='运费情况')
		taxprice=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='含税价')
		taxpointnote=models.CharField(max_length=100,default='未知',verbose_name='税点备注')
		createdatetime=models.DateTimeField(auto_now=True,verbose_name='创建时间')


		class	Meta:
				verbose_name='产品'
				verbose_name_plural=verbose_name
		def	__str__(self):
				return self.producename
class offer(models.Model):
		offerid=models.CharField(primary_key=True,max_length=100,verbose_name='报价编号')
		customer=models.CharField(max_length=100,default='未知',verbose_name='客户')
		customersource=models.CharField(max_length=100,default='未知',verbose_name='客户来源')
		country=models.CharField(max_length=100,default='未知',verbose_name='国家')
		level=models.CharField(max_length=1,default='未知',verbose_name='重要程度')
		createdatetime=models.DateTimeField(auto_now=True,null=True,verbose_name='创建时间')

		class	Meta:
				verbose_name='报价'
				verbose_name_plural=verbose_name
		def	__str__(self):
				return self.offerid
class offerdetail(models.Model):
		offer=models.ForeignKey(offer,default='未知',on_delete=models.CASCADE,verbose_name='报价单编号')
		produce=models.ForeignKey(produce,default=0,on_delete=models.CASCADE,verbose_name='产品id')
		xunnum=models.IntegerField(default=1,verbose_name='询价数量')
		beinum=models.DecimalField(max_digits=15,decimal_places=5,default=1,verbose_name='倍数')
		exchangerate=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='汇率')
		beiprice=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='倍数价格')
		profitsrmb=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='利润RMB')
		usdprice=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='美元报价')
		usdtotalprice=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='美元总价格')
		number=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='件数')
		totalvolume=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='总体积cbm')
		unitweight=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='单件抛重总体积重量(除5000)')
		totalvolumeweight=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='总体积重量')
		totalweight=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='总重量')
		createdatetime=models.DateTimeField(auto_now=True,null=True,verbose_name='创建时间')

		class	Meta:
				verbose_name='报价详情'
				verbose_name_plural=verbose_name
		def	__str__(self):
				return self.id
				
class	order_total(models.Model):
		order_id=models.CharField(max_length=20,primary_key=True,default='299912999',verbose_name='订单编号')
		customer = models.CharField(max_length=100,default='未知',verbose_name='客户姓名')
		total_am_recable_usd=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='订单总金额应收（USD）')
		total_am_recable_rmb=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='订单总金额应收（RMB）')
		total_am_received_usd=models.DecimalField(max_digits=15, decimal_places=5,default=0,null=True,blank=True,verbose_name='实收美元金额')
		total_am_received_rmb=models.DecimalField(max_digits=15, decimal_places=5,default=0,null=True,blank=True,verbose_name='实收人民币')
		exchange_rate=models.DecimalField(max_digits=15,	decimal_places=5,null=True,blank=True,default=1,verbose_name='汇率')
		Gross_profit=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='毛利')
		Bal_of_rec=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='应收账款余额')
		Bal_of_pay=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='应付账款余额')
		Profit=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='利润')
		createdatetime=models.DateTimeField(auto_now=True,verbose_name='创建时间')
		
		class	Meta:
				verbose_name='订单汇总表'
				verbose_name_plural=verbose_name
		
		

#订单号	一达通单号	类别	品名	产品单价	单位	数量	总货值	运费	税点	应付成本总金额	退税	供应商	实际付款金额	付款日期	付款方式
class	order_detail(models.Model):
		order=models.ForeignKey(order_total,default='299912999',on_delete=models.CASCADE,verbose_name='订单编号')
		product=models.ForeignKey(produce,default='未知',on_delete=models.CASCADE,verbose_name='产品名称')
		product_num=models.IntegerField(default=1,verbose_name='数量')		
		beinum=models.DecimalField(max_digits=15,decimal_places=5,default=1,verbose_name='倍数')
		unit_price=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='报价')
		sum_price=models.DecimalField(max_digits=15, decimal_places=5,default=0,verbose_name='总价')
		
		class	Meta:
				verbose_name='订单'
				verbose_name_plural=verbose_name

		

				
class order_collection(models.Model):
		deliver_desc_choices = (
    		('已发货', '已发货'),
    		('未发货', '未发货'),)
		type_money_choices = (
			('全款', '全款'),
    		('定金', '定金'),
    		('尾款', '尾款'),)
		order=models.ForeignKey(order_total,default='99999999',on_delete=models.CASCADE,verbose_name='订单编号')
		date_of_col=models.DateField(null=True,blank=True,default='2999-12-31',verbose_name='收款日期')
		produce_desc=models.CharField(max_length=200,null=True,blank=True,default='未知',verbose_name='相关产品')
		collection_mount=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='应收款金额(USD)')
		collection_s_mount=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='实收款金额(USD)')		
		rec_account=models.CharField(max_length=70,null=True,blank=True,default='未知',verbose_name='账户')		
		credit_id=models.CharField(max_length=50,null=True,blank=True,default='00',verbose_name='信保单号')	
		ydt_id=models.CharField(max_length=50,null=True,blank=True,default='00',verbose_name='一达通单号')	
		deliver_desc=models.CharField(choices=deliver_desc_choices,max_length=50,null=True,blank=True,default='未知',verbose_name='发货情况')
		total_am_received_rmb=models.DecimalField(max_digits=15, decimal_places=5,default=0,null=True,blank=True,verbose_name='应收人民币')
		total_real_received_rmb=models.DecimalField(max_digits=15, decimal_places=5,default=0,null=True,blank=True,verbose_name='实收人民币')
		estimated_exchange_rate=models.DecimalField(max_digits=15,decimal_places=5,default=6.7,verbose_name='预估汇率')			
		real_exchange_rate=models.DecimalField(max_digits=15,decimal_places=5,default=6.7,verbose_name='实际汇率')
		collection_method=models.CharField(max_length=40,default='未知',verbose_name='收款方式')
		type_money=models.CharField(choices=type_money_choices,max_length=10,default='未知',verbose_name='款类')
		remarks=models.CharField(max_length=200,null=True,blank=True,default='无',verbose_name='备注')	

		class	Meta:
				verbose_name='收款明细'
				verbose_name_plural=verbose_name

class	order_cost(models.Model):
		ticket_yesno_choices = (
			('是', '是'),
    		('否', '否'),)
		type_money_choices = (
			('全款', '全款'),
    		('定金', '定金'),
    		('尾款', '尾款'),)
		cost_type_choices = (
			('产品', '产品'),
    		('物流', '物流'),
    		('其他', '其他'),)
		order=models.ForeignKey(order_total,default='99999999',on_delete=models.CASCADE,verbose_name='订单编号')
		payment_date=models.DateField(null=True,blank=True,default='2999-12-31',verbose_name='付款日期')
		produce_desc=models.CharField(max_length=200,null=True,blank=True,default='未知',verbose_name='相关产品')
		tax_point=models.DecimalField(max_digits=15, decimal_places=5,default=0,verbose_name='税点')
		total_amount_of_cost_payable=models.DecimalField(max_digits=15,decimal_places=5,default=0,verbose_name='应付款金额')
		actual_payment_amount=models.DecimalField(max_digits=15, decimal_places=5,default=0,verbose_name='实际付款金额')	
		invoice_amount=models.DecimalField(max_digits=15, decimal_places=5,default=0,verbose_name='发票金额')
		ticket_yesno=models.CharField(choices=ticket_yesno_choices,max_length=2,default='是',verbose_name='是否开票')	
		tax_refund=models.DecimalField(max_digits=15,	decimal_places=5,default=0,verbose_name='退税')
		supplier=models.CharField(max_length=100,default='未知',verbose_name='供应商及账号信息')			
		payment_method=models.CharField(max_length=40,default='未知',verbose_name='付款方式')
		type_money=models.CharField(choices=type_money_choices,max_length=10,default='未知',verbose_name='款类')
		cost_type=models.CharField(choices=cost_type_choices,max_length=10,default='未知',verbose_name='费用类型')
		remarks=models.CharField(max_length=200,null=True,blank=True,default='无',verbose_name='备注')

		class	Meta:
				verbose_name='付款明细'
				verbose_name_plural=verbose_name

