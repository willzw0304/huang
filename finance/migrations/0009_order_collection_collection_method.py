# Generated by Django 3.0.5 on 2020-04-07 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finance', '0008_auto_20200407_2116'),
    ]

    operations = [
        migrations.AddField(
            model_name='order_collection',
            name='collection_method',
            field=models.CharField(default='未知', max_length=40, verbose_name='收款方式'),
        ),
    ]
