from django import forms
from finance.models import *

class AddProduceForm(forms.ModelForm):
    class Meta:
        model = produce
        fields = "__all__"
    def __init__(self, *args, **kwargs):
        super(AddProduceForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
class order_collectionForm(forms.ModelForm):

    class Meta:
        model = order_collection
        fields = "__all__"

