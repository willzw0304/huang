from django.shortcuts import render,redirect,reverse,HttpResponse
from finance.models import *
from django.db import connections
from django.forms.models import model_to_dict  
import json
from django.core.serializers import serialize
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage,InvalidPage
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django import forms
from finance.forms import *
from django.utils import timezone
from django.utils.timezone import now, localtime
from django.db.models import Q,F
from decimal import Decimal



# Create your views here.
def index(request):
	return render(request,'finance/app_head.html')



# 使用form组件实现注册方式
def AddProduceView(request):
	error_msg=''
	form_obj = AddProduceForm()         # 实例化一个对象
	if request.method == "POST":
		# 实例化form对象的时候，把post提交过来的数据直接传进去
		form_obj = AddProduceForm(request.POST)
		# 调用form_obj校验数据的方法
		if form_obj.is_valid():
			form_obj.save()
			error_msg="添加成功"
	if request.method=="GET":
		id=request.GET.get("id")
		if id:
			k=produce.objects.get(id=id)
			k_dict = model_to_dict(k)
			form_obj = AddProduceForm(k_dict)
		

	content={'error_msg':error_msg,
			'form_obj':form_obj,    
	}
	return render(request, "finance/addproduce.html", content)
class SearchProduceView(ListView): 
	model=produce
	ordering = 'id'   #以id字段排序
	context_object_name = 'produce_list'  # 传到template中的list



	
	def get_queryset(self): 
		produce_list=produce.objects.all()

		producename=self.request.GET.get("producename")
		if producename:
			produce_list=produce_list.filter(producename__contains=producename)
		return produce_list
	

	def get_context_data(self, **kwargs):
		context = super(SearchProduceView, self).get_context_data(**kwargs)
		context['params'] = [f for f in produce._meta.fields]        
		return context

def deleteproduce(request):
	if request.method=="GET":
		id=request.GET.get("id")
		produce.objects.filter(id=id).delete()

	return redirect(reverse('finance:searchproduce'))
def changeproduce(request,nid):
	k=produce.objects.get(id=nid)
	error_msg=''
	form_obj = AddProduceForm()         # 实例化一个对象
	if request.method == "POST":
		# 实例化form对象的时候，把post提交过来的数据直接传进去
		form_obj = AddProduceForm(request.POST,instance=k)
		# 调用form_obj校验数据的方法
		if form_obj.is_valid():
			form_obj.save()
			error_msg="修改成功"
	if request.method=="GET":
			form_obj = AddProduceForm(instance=k)
		

	content={'error_msg':error_msg,
	'form_obj':form_obj,
	'nid':nid,    
	}
	return render(request, "finance/changeproduce.html", content)
class AddofferView(ListView): 
	model=produce
	ordering = 'id'   #以id字段排序
	context_object_name = 'produce_list'  # 传到template中的list
	template_name = "finance/add_offer_list.html"  #渲染页面



	
	def get_queryset(self): 
		produce_list=produce.objects.filter(id=0)
		list_pids=self.request.GET.getlist("chb")
		pid=self.request.GET.get("pid")
		pids=self.request.GET.get("pids")
		if pid:
				if pids!='':
					list_pids=pids.split(',')                
				else:
					list_pids=[]


				if ',' in pid:
					list_pid=pid.split(',')
					list_pids=list_pids+list_pid
				else:
					list_pids.append(pid)

		if not list_pids:
				list_pids=[]
			#去重
		list_pids=list(set(list_pids))
			#排序
		list_pids = list(map(int, list_pids))
		list_pids.sort()
		produce_list=produce.objects.filter(id__in=list_pids)
				
			
		return produce_list
	

	def get_context_data(self, *args,**kwargs):
		context = super(AddofferView, self).get_context_data(**kwargs)
		list_pids=self.request.GET.getlist("chb")
		pid=self.request.GET.get("pid")
		pids=self.request.GET.get("pids")
		if pid:
				if pids!='':
					list_pids=pids.split(',')                
				else:
					list_pids=[]


				if ',' in pid:
					list_pid=pid.split(',')
					list_pids=list_pids+list_pid
				else:
					list_pids.append(pid)
		if not list_pids:
				list_pids=[]


			#去重
		list_pids=list(set(list_pids))
			#排序
		list_pids = list(map(int, list_pids))
		list_pids.sort()

		pids=','.join(list(map(str, list_pids)))
	   
				
		context['pids'] = pids
		context['pid'] = pid
		return context

def dooffer(request):
	
	if request.method=="POST":
		pids=request.POST.get("pids",None)
		xunnum=request.POST.getlist('xunnum')
		beinum=request.POST.getlist('beinum')
		exchangerate=request.POST.getlist('exchangerate')
		customer=request.POST.get('customer')
		customersource=request.POST.get('customersource')
		country=request.POST.get('country')
		level=request.POST.get('level')


		if pids and xunnum and beinum and exchangerate:
			create_time=timezone.localtime()
			offerid="QU"+create_time.strftime("%Y%m%d%H%M%S")

			list_pids=pids.split(',')

			cursor = connections['default'].cursor()


			n=0
			length = len(list_pids)
			sqlstr=""
			for id,xun,bei,rate in zip(list_pids,xunnum,beinum,exchangerate):
				if xun=='' or bei=='' or rate=='' or customer=='' or customersource=='' or country=='' or level=='':
					error_msg='有必输项没有填写！'
					content={'error_msg':error_msg,}
					return render(request, "finance/error.html", content)
				n+=1
				if n==length:
					houunion=""                
				else:
					houunion=" union all "
				sqlstr=sqlstr+"select id,producename,describe,factoryprice,'{0}' as xunnum,'{1}' as beinum,'{2}' as exchangerate,round(factoryprice*{1},3) as beiprice,round((factoryprice*{1}-factoryprice)*{0},3) as profitsrmb,round(factoryprice*{1}/{2},3) as usdprice,round(factoryprice*{1}*{0}/{2},3) as usdtotalprice,round({0}.0/eachpacknum,3) as number,round(plong*pwide*phigh*{0}/(eachpacknum*1000000.0),3) as totalvolume,round(plong*pwide*phigh/5000.0,3) as unitweight,round((plong*pwide*phigh*{0})/(5000.0*eachpacknum),3) as totalvolumeweight,round(({0}.0/eachpacknum)*(weight*eachpacknum/1000.0+1),3) as totalweight,'{4}' as offerid from finance_produce where id={3} ".format(xun,bei,rate,id,offerid)
				sqlstr=sqlstr+houunion

			
			
			cursor.execute(sqlstr)
			offerdetail_list=cursor.fetchall()

			#保存报价信息
			sqlstrinsert='''insert into finance_offer(offerid,customer,customersource,country,level,createdatetime)  
						 values('{0}','{1}','{2}','{3}','{4}','{5}')'''.format(offerid,customer,customersource,country,level,timezone.localtime())
			cursor.execute(sqlstrinsert)
			sqlstrinsert='''insert into finance_offerdetail(offer_id,produce_id,xunnum,beinum,exchangerate,beiprice,profitsrmb,usdprice,usdtotalprice,number,totalvolume,unitweight,totalvolumeweight,totalweight,createdatetime)
						  select offerid,id,xunnum,beinum,exchangerate,beiprice,profitsrmb,usdprice,usdtotalprice,number,totalvolume,unitweight,totalvolumeweight,totalweight,'{1}' from ({0})'''.format(sqlstr,timezone.localtime())
			cursor.execute(sqlstrinsert)
	   


		cursor.close()
	url=reverse('finance:searchofferdetail')+"?offerid={0}".format(offerid)
	return redirect(url)
def changeoffer(request):
	error_msg=""
	if request.method=="POST":
		offerid=request.POST.get("offerid",None)
		list_pids=request.POST.getlist('produceid')
		xunnum=request.POST.getlist('xunnum')
		beinum=request.POST.getlist('beinum')
		exchangerate=request.POST.getlist('exchangerate')
		customer=request.POST.get('customer')
		customersource=request.POST.get('customersource')
		country=request.POST.get('country')
		level=request.POST.get('level')
		cursor = connections['default'].cursor()

		if offerid:
			sqlstr="update finance_offer set customer='{0}',customersource='{1}',country='{2}',level='{3}' where offerid='{4}'".format(customer,customersource,country,level,offerid)
			cursor.execute(sqlstr)
			#print(sqlstr)

			for id,xun,bei,rate in zip(list_pids,xunnum,beinum,exchangerate):
				sqlstr='''update finance_offerdetail set 
				 xunnum='{0}'
				,beinum='{1}'
				,exchangerate='{2}'
				,beiprice=(select round(factoryprice*{1},3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,profitsrmb=(select round((factoryprice*{1}-factoryprice)*{0},3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,usdprice=(select round(factoryprice*{1}/{2},3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,usdtotalprice=(select round(factoryprice*{1}*{0}/{2},3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,number=(select round({0}.0/eachpacknum,3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,totalvolume=(select round(plong*pwide*phigh*{0}/(eachpacknum*1000000.0),3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,unitweight=(select round(plong*pwide*phigh/5000.0,3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,totalvolumeweight=(select round((plong*pwide*phigh*{0})/(5000.0*eachpacknum),3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				,totalweight=(select round(({0}.0/eachpacknum)*(weight*eachpacknum/1000.0+1),3) from finance_produce where finance_produce.id=finance_offerdetail.produce_id)
				 where  offer_id='{4}' and produce_id='{3}' '''.format(xun,bei,rate,id,offerid)
				cursor.execute(sqlstr)
				#print(sqlstr)
		error_msg="保存成功"
		cursor.close()
	url=reverse('finance:searchofferdetail')+"?offerid={0}&error_msg={1}".format(offerid,error_msg)
	return redirect(url)

class SearchOfferView(ListView): 
	model=offer
	ordering = 'offerid'   #以id字段排序
	context_object_name = 'offer_list'  # 传到template中的list



	
	def get_queryset(self): 
		offer_list=offer.objects.all()

		offerid=self.request.GET.get("offerid")
		if offerid:
			offer_list=offer_list.filter(offerid__contains=offerid)
		return offer_list
	

	def get_context_data(self, **kwargs):
		context = super(SearchOfferView, self).get_context_data(**kwargs)
		context['params'] = [f for f in offer._meta.fields]        
		return context
class SearchOfferDetailView(ListView): 
	model=offerdetail
	ordering = 'id'   #以id字段排序
	context_object_name = 'offerdetail_list'  # 传到template中的list

	def get_queryset(self): 
		offerid=self.request.GET.get('offerid',None)
		offerdetail_list=offerdetail.objects.all()
		
		
		if offerid:
			print(offerid)
			offerdetail_list=offerdetail.objects.filter(offer__offerid=offerid)
			
		else:
			print('没有数据')
		return offerdetail_list
	

	def get_context_data(self, **kwargs):
		context = super(SearchOfferDetailView, self).get_context_data(**kwargs)
		offerid=self.request.GET.get("offerid")
		context['params'] = [f for f in offerdetail._meta.fields]  
		context['offerid'] =offerid
		context['offer_list']=offer.objects.get(offerid=offerid)
		context['level_list']=['1','2','3','4','5']
		context['error_msg']=self.request.GET.get("error_msg")
		return context
def deloffer(request,offerid):
	offer.objects.filter(offerid=offerid).delete()
	return redirect(reverse('finance:searchoffer'))

def delofferdetail(request):
	offerid=request.GET.get('offerid',None)
	id=request.GET.get('id',None)
	offerdetail.objects.filter(id=id).delete()
	url=reverse('finance:searchofferdetail')+"?offerid={0}".format(offerid)
	return redirect(url)


def AddOrder(request): 
	if request.method=="POST":
		pids=request.POST.getlist('produceid')
		xunnum=request.POST.getlist('xunnum')
		beinum=request.POST.getlist('beinum')
		exchangerate=request.POST.getlist('exchangerate')
		customer=request.POST.get('customer')
		customersource=request.POST.get('customersource')
		country=request.POST.get('country')
		level=request.POST.get('level')

		create_time=timezone.localtime()
		order_id="PI"+create_time.strftime("%Y%m%d%H%M%S")

		

		if customer:
			neworder=order_total.objects.create(order_id=order_id,customer=customer,createdatetime=create_time)


		if pids and xunnum and beinum and exchangerate:
			
			cursor = connections['default'].cursor()
			for id,xun,bei,rate in zip(pids,xunnum,beinum,exchangerate):
				if xun=='' or bei=='' or rate=='' or customer=='' or customersource=='' or country=='' or level=='':
					error_msg='有必输项没有填写！'
					content={'error_msg':error_msg}
					return render(request,'finance/error.html',content)
				produce1=produce.objects.get(id=id)
				neworderdetail=order_detail.objects.create(order_id=order_id,product_id=id,product_num=xun,beinum=bei,unit_price=produce1.factoryprice*Decimal(bei),sum_price=produce1.factoryprice*Decimal(bei)*Decimal(xun))
				#print(neworderdetail)
	url=reverse('finance:searchorder')
	return redirect(url)

class SearchOrderView(ListView): 
	model=order_total
	#ordering = ['-order_id']   #以id字段排序

	context_object_name = 'order_total_list'  # 传到template中的list



	
	def get_queryset(self): 
		order_total_list=order_total.objects.all().order_by('-order_id')

		order_id=self.request.GET.get("order_id")
		if order_id:
			order_total_list=order_total_list.filter(order_id=order_id)
		return order_total_list
	

	def get_context_data(self, **kwargs):
		context = super(SearchOrderView, self).get_context_data(**kwargs)
		context['params'] = [f for f in order_total._meta.fields]        
		return context

class SearchOrderDetailView(ListView): 
	model=order_detail
	ordering = '-id'   #以id字段排序
	context_object_name = 'order_detail_list'  # 传到template中的list
	template_name = "finance/order_detail.html"  #渲染页面

	def get_queryset(self): 
		order_id=self.request.GET.get('order_id',None)
		order_detail_list=order_detail.objects.all()
		
		
		if order_id:
			print(order_id)
			order_detail_list=order_detail.objects.filter(order__order_id=order_id).order_by('-id')
			
		else:
			print('没有数据')
		return order_detail_list
	

	def get_context_data(self, **kwargs):
		context = super(SearchOrderDetailView, self).get_context_data(**kwargs)
		order_id=self.request.GET.get("order_id")
		exclude_fields=('id','order')
		context['order_detail_params'] = [f for f in order_detail._meta.fields if f.name not in exclude_fields]  
		context['order_id'] =order_id
		context['order_collection_list']=order_collection.objects.filter(order__order_id=order_id)
		context['order_collection_params'] = [f for f in order_collection._meta.fields if f.name not in exclude_fields]
		context['order_cost_list']=order_cost.objects.filter(order__order_id=order_id)
		context['order_cost_params'] = [f for f in order_cost._meta.fields if f.name not in exclude_fields]    
		#context['error_msg']=self.request.GET.get("error_msg")
		return context

class OrderDetailCreate(CreateView):
    #form_class = order_collectionForm     # 表类
    model=order_detail
    fields = ['product','product_num','beinum','unit_price','sum_price']
    template_name = 'finance/create_update.html'    # 添加表对象的模板页面
    #success_url = "/finance/searchorder/?order_id={0}".format(kwargs['order_id'])	    # 成功添加表对象后 跳转到的页面
    

    def form_valid(self, form):
        form.instance.order = order_total.objects.get(pk=self.kwargs['order_id'])
        return super(OrderDetailCreate, self).form_valid(form)
    def form_invalid(self, form):        # 定义表对象没有添加失败后跳转到的页面。
        return HttpResponse("error!form is invalid")
    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url

class OrderDetailUpdate(UpdateView):
    #form_class = order_collectionForm     # 表类
    model=order_detail
    fields = ['product','product_num','beinum','unit_price','sum_price']
    template_name = 'finance/create_update.html'    # 添加表对象的模板页面

    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url
    def get_context_data(self, **kwargs):
    	context = super(OrderDetailUpdate, self).get_context_data(**kwargs)
    	context['order_id'] =self.kwargs['order_id']
    	return context

class OrderDetailDelete(DeleteView):
    model = order_detail
    template_name = 'finance/delete.html'

    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url
    def get_context_data(self, **kwargs):
    	context = super(OrderDetailDelete, self).get_context_data(**kwargs)
    	context['order_id'] =self.kwargs['order_id']
    	return context


class OrderCollectionCreate(CreateView):
    #form_class = order_collectionForm     # 表类
    model=order_collection
    fields = ['date_of_col','produce_desc','collection_mount','collection_s_mount','rec_account','credit_id','ydt_id','deliver_desc','total_am_received_rmb','total_real_received_rmb','estimated_exchange_rate','real_exchange_rate','collection_method','type_money','remarks']
    template_name = 'finance/create_update.html'    # 添加表对象的模板页面
    #success_url = "/finance/searchorder/?order_id={0}".format(kwargs['order_id'])	    # 成功添加表对象后 跳转到的页面
    

    def form_valid(self, form):
        form.instance.order = order_total.objects.get(pk=self.kwargs['order_id'])
        return super(OrderCollectionCreate, self).form_valid(form)
    def form_invalid(self, form):        # 定义表对象没有添加失败后跳转到的页面。
        return HttpResponse("error!form is invalid")
    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url

class OrderCollectionUpdate(UpdateView):
    #form_class = order_collectionForm     # 表类
    model=order_collection
    fields = ['date_of_col','produce_desc','collection_mount','collection_s_mount','rec_account','credit_id','ydt_id','deliver_desc','total_am_received_rmb','total_real_received_rmb','estimated_exchange_rate','real_exchange_rate','collection_method','type_money','remarks']
    template_name = 'finance/create_update.html'    # 添加表对象的模板页面

    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url
    def get_context_data(self, **kwargs):
    	context = super(OrderCollectionUpdate, self).get_context_data(**kwargs)
    	context['order_id'] =self.kwargs['order_id']
    	return context

class OrderCollectionDelete(DeleteView):
    model = order_collection
    template_name = 'finance/delete.html'

    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url
    def get_context_data(self, **kwargs):
    	context = super(OrderCollectionDelete, self).get_context_data(**kwargs)
    	context['order_id'] =self.kwargs['order_id']
    	return context

class OrderCostCreate(CreateView):
    #form_class = order_collectionForm     # 表类
    model=order_cost
    fields = ['payment_date','produce_desc','tax_point','total_amount_of_cost_payable','actual_payment_amount','invoice_amount','ticket_yesno','tax_refund','supplier','payment_method','type_money','cost_type','remarks']
    template_name = 'finance/create_update.html'    # 添加表对象的模板页面
    #success_url = "/finance/searchorder/?order_id={0}".format(kwargs['order_id'])	    # 成功添加表对象后 跳转到的页面
    

    def form_valid(self, form):
        form.instance.order = order_total.objects.get(pk=self.kwargs['order_id'])
        return super(OrderCostCreate, self).form_valid(form)
    def form_invalid(self, form):        # 定义表对象没有添加失败后跳转到的页面。
        return HttpResponse("error!form is invalid")
    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url

class OrderCostUpdate(UpdateView):
    #form_class = order_collectionForm     # 表类
    model=order_cost
    fields = ['payment_date','produce_desc','tax_point','total_amount_of_cost_payable','actual_payment_amount','invoice_amount','ticket_yesno','tax_refund','supplier','payment_method','type_money','cost_type','remarks']
    template_name = 'finance/create_update.html'    # 添加表对象的模板页面

    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url
    def get_context_data(self, **kwargs):
    	context = super(OrderCostUpdate, self).get_context_data(**kwargs)
    	context['order_id'] =self.kwargs['order_id']
    	return context

class OrderCostDelete(DeleteView):
    model = order_cost
    template_name = 'finance/delete.html'

    def get_success_url(self):
    	url="?order_id={0}".format(self.kwargs['order_id'])	
    	return reverse('finance:searchorderdetail')+url
    def get_context_data(self, **kwargs):
    	context = super(OrderCostDelete, self).get_context_data(**kwargs)
    	context['order_id'] =self.kwargs['order_id']
    	return context

def search_result(request):
	if request.method == "GET":
		tablename=request.GET.get('tablename')
		page=request.GET.get('page')
		query_list=request.GET.get('query_list')
		if query_list:
			query_list=eval(query_list)
			if tablename=='customer':
				if query_list['customer']:
					qs_all=customer.objects.filter(customer__contains=query_list['customer'])
		else:
			if tablename=='customer':
				qs_all=customer.objects.all().order_by('-id')
	if request.method=="POST":
		page=1
		tablename=request.POST.get('tablename')
		if tablename=='customer':
			f_customer=request.POST.get('customer')
			query_list={'customer':f_customer,}
			if query_list['customer']:
				qs_all=customer.objects.filter(customer__contains=query_list['customer'])
			else:
				qs_all=customer.objects.all().order_by('-id')

	datacount=qs_all.count

	#json_data = serialize('json', qs_all) # str
	#qs = json.loads(json_data)

	

	qs = []
	for qs_one in qs_all:
		json_dict = model_to_dict(qs_one)
		qs.append(json_dict)

	print(qs)

	
   

	if tablename=='customer':
		#查询条件
		form_fields=('customer_name')
		#显示排除字段
		exclude_fields = ('add_time')
		form_params=[f for f in customer._meta.fields if f.name in form_fields]
		params = [f for f in customer._meta.fields if f.name not in exclude_fields]

			

	content={'qs':qs,
					'query_list':query_list,
					'datacount':datacount,
					'tablename':tablename,
					'form_params':form_params,
					'params':params,}
   

	
	return render(request,'finance/search_result.html',content)

def add_order(request):
	content={}
	if request.method == "POST":
		order_id=request.POST['order_id']
		
		content={'id':order_id}

	return render(request,'finance/add_new_order.html',content)

def change_order(request):
	content={}
	if request.method == "GET":
		f_id=request.GET['id']
		content={'id':f_id}

	return render(request,'finance/search_order_result.html',content)

def delete_order(request):
	if request.method == "GET":
		f_id=request.GET['id']
		order_total.objects.filter(id=f_id).delete()
	return render(request,'finance/search_order_result.html')

def orderdetail(request):
	if request.method == "GET":
		f_id=request.GET['id']
		order_total.objects.filter(id=f_id).delete()
	return render(request,'finance/search_order_result.html')